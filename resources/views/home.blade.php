@extends('layout.main')

@section('content')
	<h1>Benvenuto!</h1>
	<div class="row">
		<div class="col-md-4 text-center">
			<h3>Fibonacci</h3>
			<form method="GET" class="form-inline" action="/redirect">
				<input type="hidden" name="type" value="fibonacci">
				<div class="form-group">
					<input type="text" class="form-control" name="numero" placeholder="Numero">
				</div>
				<button type="submit" class="btn btn-default">Invia</button>
			</form>
		</div>
		<div class="col-md-4 text-center">
			<h3>Fattoriale</h3>
			<form method="GET" class="form-inline" action="/redirect">
				<input type="hidden" name="type" value="factorial">
				<div class="form-group">
					<input type="text" class="form-control" name="numero" placeholder="Numero">
				</div>
				<button type="submit" class="btn btn-default">Invia</button>
			</form>
		</div>
		<div class="col-md-4 text-center">
			<h3>Somma di n numeri</h3>
			<form method="GET" class="form-inline" action="/redirect">
				<input type="hidden" name="type" value="sum">
				<div class="form-group">
					<input type="text" class="form-control" name="numero" placeholder="Numero">
				</div>
				<button type="submit" class="btn btn-default">Invia</button>
			</form>
		</div>
	</div>
@endsection