@extends('layout.main')

@section('content')
	<h1>I primi {{ $elements }} elementi della serie di Fibonacci sono:</h1>
	<ol>
	@foreach($result as $num)
		<li>{{ $num }}</li>
	@endforeach
	</ol>
@endsection