<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/factorial/{number}', function($number) {
	$result = 1;

	for($i = $number; $i > 0; $i--) {
		$result *= $i;
	}

	return view('factorial', ['result' => $result, 'number' => $number]);
});

Route::get('/sum/{elements}', function($elements) {
	$sum = ($elements * ($elements + 1)) / 2;

	return view('sum', ['sum' => $sum, 'elements' => $elements]);
});

Route::get('/fibonacci/{elements}', function($elements) {
	$result = array();

	for($i = $elements; $i > 0; $i--) {
		$pos = $elements - $i;

		if($pos == 0) {
			$result[$pos] = 0;
		} else if($pos == 1) {
			$result[$pos] = 1;
		} else {
			$result[$pos] = $result[$pos-1] + $result[$pos-2];
			if(is_infinite($result[$pos])) break;
		}
	}

	return view('fibonacci', ['result' => $result, 'elements' => $elements]);
});

Route::get('/redirect', function(Request $request) {
	$data = $request->all();

	return redirect("/".$data["type"]."/".$data["numero"]);
});