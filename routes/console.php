<?php

use Illuminate\Foundation\Inspiring;
use Symfony\Component\Process\Process;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('assets:publish {folder} {destFolder}', function ($folder, $destFolder) {
	$directorySplit = explode("/", $destFolder);
	$directory = "public/";

	for($i = 0; $i < count($directorySplit)-1; $i++) {
		$process = new Process("mkdir -p ".$directory."/".$directorySplit[$i]);
		$process->run();

		$directory .= $directorySplit[$i];
	}

	$process = new Process("cp -R ".$folder." public/".$directory);
	$process->run();

	$process = new Process("rm -rf public/".$destFolder."/*.map");
	$process->run();

	$this->info("The asset has been published to public/".$destFolder);
})->describe('Publish an asset from the vendor folder to the selected destination folder');

Artisan::command('assets:all', function() {
	Artisan::call('assets:publish', ['folder' => 'vendor/twbs/bootstrap/dist/fonts', 'destFolder' => 'assets/bootstrap/fonts']);
	Artisan::call('assets:publish', ['folder' => 'vendor/twbs/bootstrap/dist/css', 'destFolder' => 'assets/bootstrap/css']);
	Artisan::call('assets:publish', ['folder' => 'vendor/twbs/bootstrap/dist/js', 'destFolder' => 'assets/bootstrap/js']);
})->describe('Publish all assets defined in routes/console.php');